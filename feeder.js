let serverAddress = "ws://ipaddress"
let socket = new WebSocket(serverAddress);

function sendCMD(cmd)
{
	if(cmd == "start")
	{
		document.getElementById("startBtn").disabled = true;
		document.getElementById("endBtn").disabled = false;
	}
	if(cmd == "end")
	{
		document.getElementById("startBtn").disabled = false;
		document.getElementById("endBtn").disabled = true;
	}
	socket.send(cmd);
}
