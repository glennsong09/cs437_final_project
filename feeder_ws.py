import RPi.GPIO as GPIO
import time
from readchar import readkey, key
import asyncio
import websockets
import threading

OFFSE_DUTY = 0.5        #define pulse offset of servo
SERVO_MIN_DUTY = 2.5+OFFSE_DUTY     #define pulse duty cycle for minimum angle of servo
SERVO_MAX_DUTY = 12.5+OFFSE_DUTY    #define pulse duty cycle for maximum angle of servo
servoPin = 12

def map( value, fromLow, fromHigh, toLow, toHigh):  # map a value from one range to another range
    return (toHigh-toLow)*(value-fromLow) / (fromHigh-fromLow) + toLow

def setup():
    global p
    GPIO.setmode(GPIO.BOARD)         # use PHYSICAL GPIO Numbering
    GPIO.setup(servoPin, GPIO.OUT)   # Set servoPin to OUTPUT mode
    GPIO.output(servoPin, GPIO.LOW)  # Make servoPin output LOW level
    p = GPIO.PWM(servoPin, 50)     # set Frequece to 50Hz
    p.start(0)                     # Set initial Duty Cycle to 0
    

def servoWrite(angle):      # make the servo rotate to specific angle, 0-180 
    if(angle<10):
        angle = 10
    elif(angle > 180):
        angle = 180
    duty = map(angle,0,180,SERVO_MIN_DUTY,SERVO_MAX_DUTY)
    #duty = 2 + angle/18
    p.ChangeDutyCycle(duty) # map the angle to duty cycle and output it
    
def start():
    servoWrite(100)     # Write dc value to servo
    time.sleep(0.5)

def stop():
    servoWrite(0)
    time.sleep(0.5)

def feed():
	start()
	time.sleep(3)
	stop()

def destroy():
    p.stop()
    GPIO.cleanup()

async def echo(websocket):
    async for message in websocket:
        if message == 'start':
            print("START_COMMAND")
            start()
        if message == 'end':
            print('END_COMMAND')
            stop()
async def main():
    async with websockets.serve(echo, "ipaddress", 8765):
        await asyncio.Future()  # run forever

def thread_server_function(name):

    try:
        asyncio.run(main())
    except error:
        print(error)

if __name__ == '__main__':     # Program entrance
    print ('Program is starting..., q to stop, up key to start, down key to end, f to feed')
    setup()
    #stop()

x1 = threading.Thread(target=thread_server_function, args=(1,), daemon=True)
x1.start()

while True:
    k = readkey() # & 0xFF
    if k == key.ESC or k == 'q':
        destroy()
        break
    if k == key.UP:
        print("Up")		
        start()
    if k == key.DOWN:
        print("DOWN")		
        stop()
    if k == 'f':
        print("Feed")		
        feed()
